#!/bin/bash

export DEBIAN_FRONTEND=noninteractive

curl -sSl https://get.docker.com | sh

usermod -aG docker $(whoami)

sudo curl -L "https://github.com/docker/compose/releases/download/1.26.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose

mkdir /srv/docker
